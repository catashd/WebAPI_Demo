﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IRepositoryBase<T> where T : class
    {
        T Get(object id);

        T Load(object id);

        object Save(T entity);
    }
}