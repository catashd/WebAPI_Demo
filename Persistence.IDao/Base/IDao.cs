﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.IDao
{
    public interface IDao<T> where T : class
    {
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T Get(object id);

        T Load(object id);

        object Save(T entity);

        /// <summary>
        ///     修改实体
        /// </summary>
        /// <param name= entity>实体</param>
        void Update(T entity);

        /// <summary>
        ///     修改或保存实体
        /// </summary>
        /// <param name= entity>实体</param>
        void SaveOrUpdate(T entity);

        /// <summary>
        ///     修改或保存实体
        /// </summary>
        /// <param name= entity>实体</param>
        object SaveOrUpdateCopy(T entity);

        /// <summary>
        ///     修改或保存实体
        /// </summary>
        /// <param name= entity>实体</param>
        object Merge(T entity);

        /// <summary>
        ///     删除实体
        /// </summary>
        /// <param name= id>ID</param>
        void Delete(object id);

        /// <summary>
        ///     删除实体
        /// </summary>
        /// <param name= idList>ID集合</param>
        void DeleteList(IList<object> idList);

        /// <summary>
        ///     获取全部集合
        /// </summary>
        /// <returns>集合</returns>
        IQueryable<T> LoadAll();

        /// <summary>
        /// 分页获取全部集合
        /// </summary>
        /// <param name= count>记录总数</param>
        /// <param name= pageIndex>页码</param>
        /// <param name= pageSize>每页大小</param>
        /// <returns>集合</returns>
        IQueryable<T> LoadAllWithPage(out long count, int pageIndex, int pageSize);

        /// <summary>
        /// 分页获取全部集合 + 排序
        /// </summary>
        /// <param name="count">记录总数</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">每页大小</param>
        /// <param name="propertyName">排序的属性</param>
        /// <returns></returns>
        IQueryable<T> LoadAllWithPageOrderBy(out long count, int pageIndex, int pageSize, string propertyName, bool IsDesc = false);

        /// <summary>
        ///     批量保存
        /// </summary>
        /// <param name= objects></param>
        void Save(IList<T> objects);

        /// <summary>
        ///     批量更新
        /// </summary>
        /// <param name= objects></param>
        void Update(IList<T> objects);

        /// <summary>
        ///     批量删除
        /// </summary>
        /// <param name= objects></param>
        void Delete(IList<T> objects);
    }
}