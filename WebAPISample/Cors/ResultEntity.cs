﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPISample.Cors
{
    public class ResultEntity
    {
        public static Domain.Entities.ResultModel<T> CreateResultEntity<T>()
        {
            return new Domain.Entities.ResultModel<T>()
            {
                IsSucess = true,
                Message = string.Empty,
                CallbackMethod = string.Empty,
                Data = default(T)
            };
        }
    }
}