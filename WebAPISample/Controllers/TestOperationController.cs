﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPISample.Cors;
using System.Reflection;

namespace WebAPISample.Controllers
{
    [RequestAuthorize]
    public class TestOperationController : ApiController
    {
        protected Domain.Operation.IOperationServices operationServices = new Domain.Operation.OperationServices();

        #region Test

        /// <summary>
        /// AddTestModel
        /// </summary>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddTestModel")]
        public HttpResponseMessage AddTestModel(dynamic jsonData)
        {
            string method = MethodBase.GetCurrentMethod().Name;

            var item = Newtonsoft.Json.JsonConvert.DeserializeObject<Domain.Entities.OperModel>(Convert.ToString(jsonData));
            try
            {
                item.result = item.x + item.y;
                item.Id = CommHelper.KeyNumber.CreateTablePrimaryKey_Guid();
                item.Operation = method;
                operationServices.SaveOperModel(item);
            }
            catch (Exception ex)
            {
            }

            var strJson = Newtonsoft.Json.JsonConvert.SerializeObject(item);
            var resp = new HttpResponseMessage { Content = new StringContent(strJson, System.Text.Encoding.UTF8, "application/json") };

            return resp;
        }

        /// <summary>
        /// GetTestModels
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTestModels")]
        [AllowAnonymous]
        public HttpResponseMessage GetTestModels()
        {
            var resultEntity = ResultEntity.CreateResultEntity<List<Domain.Entities.OperModel>>();
            try
            {
                var list = operationServices.GetOperModels().ToList();
                resultEntity.Data = list;
            }
            catch (Exception ex)
            {
                resultEntity.Message = ex.Message;
                resultEntity.IsSucess = false;
            }

            var strJson = Newtonsoft.Json.JsonConvert.SerializeObject(resultEntity);

            var resp = new HttpResponseMessage { Content = new StringContent(strJson, System.Text.Encoding.UTF8, "application/json") };

            return resp;
        }

        /// <summary>
        /// UpdateModels
        /// </summary>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("UpdateModel")]
        public HttpResponseMessage UpdateModel(dynamic jsonData)
        {
            string method = MethodBase.GetCurrentMethod().Name;
            var resultEntity = ResultEntity.CreateResultEntity<Domain.Entities.OperModel>();

            try
            {
                var item = Newtonsoft.Json.JsonConvert.DeserializeObject<Domain.Entities.OperModel>(Convert.ToString(jsonData));
                item.Operation = method;
                if (operationServices.UpdateOperModel(item))
                {
                    resultEntity.Data = item;
                }
                else
                {
                    resultEntity.IsSucess = false;
                }
            }
            catch (Exception ex)
            {
                resultEntity.Message = ex.Message;
                resultEntity.IsSucess = false;
            }

            var strJson = Newtonsoft.Json.JsonConvert.SerializeObject(resultEntity);
            var resp = new HttpResponseMessage { Content = new StringContent(strJson, System.Text.Encoding.UTF8, "application/json") };

            return resp;
        }

        /// <summary>
        /// UpdateModels
        /// </summary>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteModels")]
        public HttpResponseMessage DeleteModels(dynamic jsonData)
        {
            string method = MethodBase.GetCurrentMethod().Name;
            var resultEntity = ResultEntity.CreateResultEntity<List<Domain.Entities.OperModel>>();

            try
            {
                List<Domain.Entities.OperModel> item = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Domain.Entities.OperModel>>(Convert.ToString(jsonData));
                item.ForEach(x => x.Operation = method);
                if (operationServices.DeleteOperModels(item))
                {
                    resultEntity.Data = item;
                }
                else
                {
                    resultEntity.IsSucess = false;
                }
            }
            catch (Exception ex)
            {
                resultEntity.Message = ex.Message;
                resultEntity.IsSucess = false;
            }

            var strJson = Newtonsoft.Json.JsonConvert.SerializeObject(resultEntity);
            var resp = new HttpResponseMessage { Content = new StringContent(strJson, System.Text.Encoding.UTF8, "application/json") };

            return resp;
        }

        #endregion Test
    }
}