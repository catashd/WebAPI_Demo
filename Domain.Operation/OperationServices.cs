﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Repository;

namespace Domain.Operation
{
    public class OperationServices : IOperationServices
    {
        private IOperModelRepository _OperModelRepository = (IOperModelRepository)Spring.Context.Support.ContextRegistry.GetContext().GetObject("OperModelRepository");

        public bool SaveOperModel(OperModel entity)
        {
            return _OperModelRepository.SaveOperModel(entity);
        }

        public IList<OperModel> GetOperModels()
        {
            return _OperModelRepository.GetOperModels();
        }

        public bool UpdateOperModel(Entities.OperModel entity)
        {
            return _OperModelRepository.UpdateOperModel(entity);
        }

        public bool DeleteOperModels(IList<Entities.OperModel> entities)
        {
            return _OperModelRepository.DeleteOperModels(entities);
        }
    }
}