﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Dao
{
    public class OperModelDao : IDao.DaoBase<Persistence.Entities.OperModel>, IDao.IOperModelDao
    {
        public bool SaveOperModel(Persistence.Entities.OperModel entity)
        {
            base.Save(entity);
            return true;
        }

        public IList<Entities.OperModel> GetOperModels()
        {
            return base.LoadAll().ToList();
        }

        public bool UpdateOperModel(Persistence.Entities.OperModel entity)
        {
            base.SaveOrUpdate(entity);
            return true;
        }

        public bool DeleteOperModels(IList<Persistence.Entities.OperModel> entities)
        {
            base.Delete(entities);
            return true;
        }
    }
}