﻿using CommHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using System.Windows.Forms;
using WebAPISample.Controllers;

namespace FrmTest
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
            this.btnAdd.Click += btnAdd_Click;
            this.btnGetAll.Click += btnGetAll_Click;
            this.btnUpdate.Click += btnUpdate_Click;
            this.btnDelete.Click += btnDelete_Click;
            Login();
        }

        private Domain.Entities.ResultModel<string> token = null;

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteOperModels();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateOperModel();
        }

        private void btnGetAll_Click(object sender, EventArgs e)
        {
            GetTestModels();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddTestModel();
        }

        private void CreatListItem(string strMethod, Domain.Entities.OperModel item)
        {
            lbxCommand.Items.Add(new myListItem(string.Format("{0} 【{1}】数据测试成功 {2} {3} {4}！", item.Id, strMethod, item.x, item.Operation, item.y), item.Id, item));
        }

        private void ListItemClear()
        {
            lbxCommand.Items.Clear();
        }

        private void AddTestModel()
        {
            if (null != token && token.IsSucess)
            {
                Domain.Entities.OperModel operModel = new Domain.Entities.OperModel { x = 1, y = 2 };

                string url = string.Format("{0}/AddTestModel", Url);
                string post = HttpWebHelper.RequestPost(url, operModel, Encoding.UTF8, token.Data);

                var item = Newtonsoft.Json.JsonConvert.DeserializeObject<Domain.Entities.OperModel>(post);

                if (this.IsHandleCreated && !this.IsDisposed)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        CreatListItem("新增", item);
                    }));
                }
            }
        }

        private void GetTestModels()
        {
            string url = string.Format("{0}/GetTestModels", Url);
            string post = HttpWebHelper.RequestGet(url, Encoding.UTF8);

            var entity = Newtonsoft.Json.JsonConvert.DeserializeObject<Domain.Entities.ResultModel<List<Domain.Entities.OperModel>>>(post);

            if (entity.IsSucess)
            {
                if (this.IsHandleCreated && !this.IsDisposed)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        ListItemClear();
                        foreach (var item in entity.Data)
                        {
                            CreatListItem("获取", item);
                        }
                    }));
                }
            }
        }

        private void DeleteOperModels()
        {
            if (null != lbxCommand.SelectedItem && null != token && token.IsSucess)
            {
                var operModel = (lbxCommand.SelectedItem as myListItem).Tag as Domain.Entities.OperModel;
                var operModels = new List<Domain.Entities.OperModel>();
                operModels.Add(operModel);
                string url = string.Format("{0}/DeleteModels", Url);
                string post = HttpWebHelper.RequestPost(url, operModels, Encoding.UTF8, token.Data, type: "DELETE");

                var entity = Newtonsoft.Json.JsonConvert.DeserializeObject<Domain.Entities.ResultModel<List<Domain.Entities.OperModel>>>(post);

                if (entity.IsSucess)
                {
                    if (this.IsHandleCreated && !this.IsDisposed)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            foreach (var item in entity.Data)
                            {
                                lbxCommand.Items.RemoveAt(lbxCommand.SelectedIndex);
                                CreatListItem("删除", item);
                            }
                        }));
                    }
                }
            }
        }

        private void UpdateOperModel()
        {
            if (null != lbxCommand.SelectedItem && null != token && token.IsSucess)
            {
                var operModel = (lbxCommand.SelectedItem as myListItem).Tag as Domain.Entities.OperModel;

                string url = string.Format("{0}/UpdateModel", Url);
                string post = HttpWebHelper.RequestPost(url, operModel, Encoding.UTF8, token.Data, type: "PUT");

                var entity = Newtonsoft.Json.JsonConvert.DeserializeObject<Domain.Entities.ResultModel<Domain.Entities.OperModel>>(post);

                if (entity.IsSucess)
                {
                    if (this.IsHandleCreated && !this.IsDisposed)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            lbxCommand.Items.RemoveAt(lbxCommand.SelectedIndex);

                            CreatListItem("更新", entity.Data);
                        }));
                    }
                }
            }
        }

        private void Login()
        {
            try
            {
                string post = HttpWebHelper.RequestGet(string.Format("{0}/Login", Url), Encoding.UTF8);
                var entity = Newtonsoft.Json.JsonConvert.DeserializeObject<Domain.Entities.ResultModel<string>>(post);

                token = entity;
            }
            catch (Exception e)
            {
            }
        }

        public class myListItem
        {
            public string Name
            { get; set; }

            public string Value  //可以存放其他属性（int型）
            { get; set; }

            public object Tag
            { get; set; }       //存放其他属性(object型)

            public override string ToString()
            { return Name; }

            public myListItem(string name, string value, object tag)
            { Name = name; Value = value; Tag = tag; }
        }

        private string Url
        {
            get
            {
                return string.Format("http://{0}:45957", "localhost");
            }
        }
    }
}